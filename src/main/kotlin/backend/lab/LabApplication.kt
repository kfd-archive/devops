package backend.lab

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
class LabApplication {
    @RestController
    class Controller {
        @GetMapping
        fun index() = "Hello World!"
    }
}

fun main(args: Array<String>) {
    runApplication<LabApplication>(*args)
}
